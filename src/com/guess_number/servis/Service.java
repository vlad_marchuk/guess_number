package com.guess_number.servis;

import java.util.Random;
import java.util.Scanner;

public class Service {
    Scanner scanner = new Scanner(System.in);
    Random random = new Random();


    public int[] starGame (int min,int max,int numberOfAttempts){
        if (min < 1|max > 200 | numberOfAttempts < 1 | numberOfAttempts > 15){
            System.out.println("Проблемка введенные данные должны быть: не отрицательные," +
                    " минимум = 1, максимум = 200) и количества попыток " +
                    " минимум = 1, максимум = 15).");
            System.exit(0);
        }
        int diff = max - min;
        int secretNumber = random.nextInt(diff + 1);
        secretNumber += min;
        System.out.println("Привет, я загадал число в диапазоне от "+ min +" до "+ max + " .Попробуй угадать его за "+ numberOfAttempts +" попыток!” ");
        int[] myArr = new int[]{secretNumber,numberOfAttempts};
        return myArr;
    }

    public void guessingFirstTry(int secretNumber,int numberOfAttempts){
        System.out.println("Попробуй угадать с первого раза, а вдруг у тебя получится :-) но это врятли.Введи свое число и нажми Entrer");
        Scanner scanner = new Scanner(System.in);
        int tryingToGuess = checkExitScan();
        if (secretNumber == tryingToGuess ){
            System.out.println("Победа , поздравляю !!!");
        }else
            helpToGuess(tryingToGuess,secretNumber);
        System.out.println("У тебя осталось "+ (numberOfAttempts - 1) + " попиток");
    }

    public void guessingAfterFirstTry(int secretNumber,int numberOfAttempts){

         for(int i = 1; i <= numberOfAttempts; i++) {
             System.out.println("Введи свое число и нажми Entrer.");
             int tryingToGuess = checkExitScan();
             if (secretNumber == tryingToGuess ){
                 System.out.println("Победа , поздравляю !!! Ты угадал за "+ ( i+1) + " попиток");
                 System.exit(0);
             }else
                 helpToGuess(tryingToGuess,secretNumber);
             System.out.println("У тебя осталось "+ (numberOfAttempts - i) + " попиток");
         }
         System.out.println("Мдааа , ну шо я могу тебе сказать , не фартовый ");
     }

     public void helpToGuess (int tryingToGuess,int secretNumber) {

         int diff = Math.abs(secretNumber - tryingToGuess);

         if (diff >= 15) {
             System.out.println("Холодно , даже очень холодно");
         }
         if (diff < 15 & diff > 10) {
             System.out.println("Холодно , но не сильно");
         }
         if (diff <= 10 & diff > 5) {
             System.out.println("Теплее");
         }
         if (diff <= 5) {
             System.out.println("Очень тепло , прям таки подгорает");
         }
     }

     public int checkExitScan(){
         int checkNum = 0;
        String scanExit = scanner.nextLine();
        String exit = "exit";
        if (exit.equals(scanExit)) {
            System.out.println("Пока, хорошего тебе дня!!!");
            System.exit(0);
        }
        return  checkNum = Integer.parseInt(scanExit);
    }
}
