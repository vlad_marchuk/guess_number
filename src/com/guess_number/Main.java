package com.guess_number;

import com.guess_number.servis.Service;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Service service = new Service();
        Scanner scanner = new Scanner(System.in);
        //////////// Старт игры

        int [] secretNumberAndAtemps = service.starGame(1,150,10);
        int secretNumber = secretNumberAndAtemps[0];
        int numberOfAttempts = secretNumberAndAtemps[1]; ;

        service.guessingFirstTry(secretNumber,numberOfAttempts);

        numberOfAttempts = numberOfAttempts - 1;

        service.guessingAfterFirstTry(secretNumber,numberOfAttempts);
    }
}

